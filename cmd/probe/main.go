// Command probe provides response time statistics of target URL endpoint.
package main

import (
	"flag"
	"fmt"
	"net/url"
	"os"
	"strings"
	"time"

	"gitlab.com/jaguirre/probe/pkg/probe/http"
	"gitlab.com/jaguirre/probe/pkg/stats"
)

func main() {
	var (
		prober  http.Prober
		verbose bool
	)

	flag.Int64Var(&prober.Count, "c", 0, "count - Stop after sending and receiving count responses. If this option is not specified, it will operate until interrupted.")
	flag.DurationVar(&prober.Wait, "i", 10*time.Second, "wait - Wait wait duration between sending each request.")
	flag.DurationVar(&prober.Timeout, "t", 5*time.Minute, "timeout - Specify a timeout for each request to URL.")
	flag.BoolVar(&verbose, "v", false, "verbose - Specify whether to print details of each progin timings.")

	flag.Usage = usage
	flag.Parse()

	args := flag.Args()
	if len(args) == 0 {
		flag.Usage()
		os.Exit(1)
	}

	u, err := url.Parse(args[0])
	if err != nil {
		fmt.Fprintf(os.Stderr, "invalid target url: %v", err)
		os.Exit(1)
	}

	op := fmt.Sprintf("Probe %q", u.String())
	m := make(metrics)
	for res := range prober.Run(u.String()) {
		t, err := res.Trace, res.Err
		if err != nil {
			fmt.Fprintf(os.Stderr, "%s error: %v\n", op, res.Err)
			continue
		}

		fmt.Printf("%s %s: %v", op, t.Status(), t.ElapsedTime())
		if verbose {
			fmt.Printf("\n\t* Resolve=%v", t.ResolveTime())
			m.Add("resolve", t.ResolveTime())

			fmt.Printf("\n\t* TLS=%v", t.TLSTime())
			m.Add("tls", t.TLSTime())

			fmt.Printf("\n\t* Connect=%v", t.ConnectTime())
			m.Add("connect", t.ConnectTime())

			fmt.Printf("\n\t* Processing=%v", t.ProcessingTime())
			m.Add("processing", t.ProcessingTime())

			fmt.Printf("\n\t* Transfer=%v", t.TransferTime())
			m.Add("transfer", t.TransferTime())
		}
		fmt.Printf("\n")

		m.Add("round-trip", t.ElapsedTime())
	}

	fmt.Printf("\n--- %s statistics (min/avg/max/stddev) ---\n", op)
	for k, s := range m {
		fmt.Printf("%10s %v/%v/%v/%v (%d samples)\n", strings.Title(k), s.Min(), s.Mean(), s.Max(), s.Stddev(), s.Count())
	}
}

func usage() {
	fmt.Fprintf(os.Stderr, "\nThe probe utiltity collect response time statistics of probings against a target URL.\n")
	fmt.Fprintf(os.Stderr, "\nUSAGE: probe [OPTIONS] TARGET\n")
	fmt.Fprintf(os.Stderr, "\nOPTIONS:\n\n")
	flag.PrintDefaults()
}

type metrics map[string]*stats.Stats

func (m metrics) Add(key string, sample time.Duration) {
	s, ok := m[key]
	if !ok {
		s = new(stats.Stats)
		m[key] = s
	}
	s.Add(sample)
}
