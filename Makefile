REPO_PATH := gitlab.com/jaguirre/probe
BIN_DIR   := bin
CMDS      := $(shell ls cmd)

.PHONY: all
all: test build

.PHONY: build
build: $(CMDS)

$(CMDS):
	@echo "BUILD: ${REPO_PATH}/cmd/${@}"
	@go build -o $(BIN_DIR)/$@ $(REPO_PATH)/cmd/$@

TEST_PKGS :=  $(shell go list ./...)

.PHONY: test
test: $(TEST_PKGS)

$(TEST_PKGS):
	@echo "LINT: ${@}"
	@go vet $@
	@echo "TEST: ${@}"
	@go test -race -cover -v $@

