// Package stats defines Stats type that is used to store
// simple statistics of a set of samples.
package stats

import (
	"math"
	"time"
)

// Stats stores simple statistics of a set of samples.
type Stats struct {
	min   time.Duration // min sample value
	max   time.Duration // max sample value
	count int64         // number of samples

	// variables to compute online mean and variance using
	// Welford algorithm.
	mean time.Duration
	vari time.Duration
}

// Add adds samples to Stats.
func (s *Stats) Add(samples ...time.Duration) {
	for i := range samples {
		s.update(samples[i])
	}
}

func (s *Stats) update(v time.Duration) {
	s.count++
	if v > s.max || s.count == 1 {
		s.max = v
	}
	if v < s.min || s.count == 1 {
		s.min = v
	}
	delta := v - s.mean
	s.mean += time.Duration(float64(delta) / float64(s.count))
	delta2 := v - s.mean
	s.vari += delta * delta2
}

// Max returns the max sample value
func (s *Stats) Max() time.Duration { return s.max }

// Min returns the min samples value
func (s *Stats) Min() time.Duration { return s.min }

// Mean returns the mean of the samples.
func (s *Stats) Mean() time.Duration { return s.mean }

// Stddev returns the standar deviation of the samples.
func (s *Stats) Stddev() time.Duration {
	var vari time.Duration
	if s.count > 1 {
		vari = time.Duration(float64(s.vari) / float64(s.count-1))
	}
	return time.Duration(math.Sqrt(float64(vari)))
}

// Count returns the number of the samples.
func (s *Stats) Count() int64 { return s.count }
