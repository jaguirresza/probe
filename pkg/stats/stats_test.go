package stats_test

import (
	"math"
	"math/rand"
	"testing"
	"time"

	"gitlab.com/jaguirre/probe/pkg/stats"
)

type statsValues struct {
	min    time.Duration
	max    time.Duration
	mean   time.Duration
	stddev time.Duration
	count  int64
}

type testCase struct {
	sample time.Duration
	exp    statsValues
}

func testStats(t *testing.T, testCases []testCase) {
	const eps = 50 * time.Microsecond

	var s stats.Stats
	for _, tc := range testCases {
		s.Add(tc.sample)

		if s.Count() != tc.exp.count {
			t.Fatalf("stats.Add: unexpected Count; exp: %v, got: %v", tc.exp.count, s.Count())
		}

		diff := abs(s.Min() - tc.exp.min)
		if diff != 0 {
			t.Fatalf("stats.Add: unexpected Min; exp: %v, got: %v, diff: %v", tc.exp.min, s.Min(), diff)
		}
		diff = abs(s.Max() - tc.exp.max)
		if diff != 0 {
			t.Fatalf("stats.Add: unexpected Max; exp: %v, got: %v, diff: %v", tc.exp.max, s.Max(), diff)
		}
		// Ensure we don't have an error greater than a eps
		diff = abs(s.Mean() - tc.exp.mean)
		if diff > eps {
			t.Fatalf("stats.Add: unexpected Mean; exp: %v, got: %v, diff: %v", tc.exp.mean, s.Mean(), diff)
		}
		// Ensure we don't have an error greater than a eps
		diff = abs(s.Stddev() - tc.exp.stddev)
		if diff > eps {
			t.Fatalf("stats.Add: unexpected Stddev; exp: %v, got: %v, diff: %v", tc.exp.stddev, s.Stddev(), diff)
		}
	}
}

func TestAdd(t *testing.T) {
	const numSamples = 1e3

	t.Run("Count", func(t *testing.T) {
		t.Parallel()

		testCases := make([]testCase, 0, numSamples)
		for i := 0; i < numSamples; i++ {
			testCases = append(testCases, testCase{
				sample: 1 * time.Nanosecond,
				exp: statsValues{
					min:    1 * time.Nanosecond,
					max:    1 * time.Nanosecond,
					mean:   1 * time.Nanosecond,
					stddev: 0,
					count:  int64(i + 1),
				},
			})
		}

		testStats(t, testCases)
	})
	t.Run("Min/Mean/Max/Stddev", func(t *testing.T) {
		t.Parallel()

		samples := make([]time.Duration, 0, numSamples)
		for i := 0; i < numSamples; i++ {
			samples = append(samples, time.Duration(rand.Intn(int(time.Hour))))
		}

		testCases := make([]testCase, 0, numSamples)

		min, max := samples[0], samples[0]
		var sum time.Duration
		for i, s := range samples {
			if s < min {
				min = s
			}
			if s > max {
				max = s
			}

			sum += s
			mean := time.Duration(float64(sum) / float64(i+1))

			// Compute variance using two pass algorithm
			var vari time.Duration
			if i > 0 {
				for j := 0; j <= i; j++ {
					delta := (samples[j] - mean)
					vari += delta * delta
				}
				vari = time.Duration(float64(vari) / float64(i))
			}

			testCases = append(testCases, testCase{
				sample: s,
				exp: statsValues{
					min:    min,
					max:    max,
					mean:   mean,
					stddev: time.Duration(math.Sqrt(float64(vari))),
					count:  int64(i + 1),
				},
			})
		}

		testStats(t, testCases)
	})
}

func abs(d time.Duration) time.Duration {
	if d < 0 {
		return -1 * d
	}
	return d
}
