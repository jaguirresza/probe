package probe

import "fmt"

// Status identifies the status of a probing.
type Status uint8

const (
	// Unknown identifies that the status of the probing is unknown.
	Unknown Status = iota
	// Success identifies a success status of a probing.
	Success
	// Invalid identifies a client error of a probing.
	Invalid
	// Failure identifies a failure status of a probing.
	Failure
)

func (s Status) String() string {
	switch s {
	case Unknown:
		return "unknown"
	case Success:
		return "success"
	case Invalid:
		return "invalid"
	case Failure:
		return "failure"
	default:
		return fmt.Sprintf("Status(%d)", s)
	}
}
