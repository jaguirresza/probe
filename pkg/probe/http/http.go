// Package http provides types and utilities to HTTP probe and collect timing
// against target URLs.
package http

import (
	"context"
	"net/http"
	"net/http/httptrace"
	"strings"
	"time"

	"gitlab.com/jaguirre/probe/pkg/probe"
)

// Trace holds response timings for a single HTTP round trip and his status
type Trace struct {
	start         time.Time
	dnsDone       time.Time
	connectDone   time.Time
	gotConn       time.Time
	responseStart time.Time
	end           time.Time

	status probe.Status
	tls    bool
}

// Status returns the status of the trace's probe.
func (t *Trace) Status() probe.Status { return t.status }

// ElapsedTime returns the HTTP response time of the probe.
func (t *Trace) ElapsedTime() time.Duration { return t.end.Sub(t.start) }

// ResolveTime returns the DNS resolution time of the probe.
func (t *Trace) ResolveTime() time.Duration { return t.dnsDone.Sub(t.start) }

// TLSTime returns the TLS time of the probe. If the probe was not using TLS
// then TLSTime is zero.
func (t *Trace) TLSTime() time.Duration {
	if !t.tls {
		return 0
	}
	return t.gotConn.Sub(t.dnsDone)
}

// ConnectTime retuns the connection time of the probe.
func (t *Trace) ConnectTime() time.Duration {
	if !t.tls {
		return t.gotConn.Sub(t.dnsDone)
	}
	return t.connectDone.Sub(t.dnsDone)
}

// ProcessingTime retuns the connection time of the probe.
func (t *Trace) ProcessingTime() time.Duration {
	return t.responseStart.Sub(t.gotConn)
}

// TransferTime retuns the connection time of the probe.
func (t *Trace) TransferTime() time.Duration {
	return t.end.Sub(t.responseStart)
}

// transport is a custom RoundTripper that closes any open connection after
// each round trip and keep track of timmings of each HTTP roundtrip.
type transport struct {
	*http.Transport
	trace *Trace
}

func newTransport(trace *Trace) http.RoundTripper {
	return &transport{
		Transport: http.DefaultTransport.(*http.Transport),
		trace:     trace,
	}
}
func (t *transport) RoundTrip(req *http.Request) (*http.Response, error) {
	if req.URL.Scheme == "https" {
		t.trace.tls = true
	}

	defer func() {
		t.CloseIdleConnections() // ensure no connection will be reused
		t.trace.end = time.Now()
	}()

	ctx := httptrace.WithClientTrace(req.Context(), &httptrace.ClientTrace{
		DNSStart: func(httptrace.DNSStartInfo) { t.trace.start = time.Now() },
		DNSDone:  func(httptrace.DNSDoneInfo) { t.trace.dnsDone = time.Now() },
		ConnectStart: func(network, addr string) {
			// No DNS resolution
			if t.trace.dnsDone.IsZero() {
				t.trace.start = time.Now()
				t.trace.dnsDone = t.trace.start
			}
		},
		ConnectDone:          func(network, addr string, err error) { t.trace.connectDone = time.Now() },
		GotConn:              func(httptrace.GotConnInfo) { t.trace.gotConn = time.Now() },
		GotFirstResponseByte: func() { t.trace.responseStart = time.Now() },
	})

	return t.Transport.RoundTrip(req.WithContext(ctx))
}

// Result describes a HTTP probing of a Prober.
type Result struct {
	Trace *Trace
	Err   error
}

// A Prober manages the runs of multiple HTTP probings.
type Prober struct {
	// Count specifies the number of HTTP probings will be made.
	// If zero then Prober will run forever.
	Count int64
	// Wait specifies the duration between sending each request.
	Wait time.Duration
	// Timeout specifies the time to wait for each probing to complete.
	Timeout time.Duration

	count int64 // keep count of the number of requests made.
}

// Run runs multiple HTTP probings against a target URL, allowing the caller
// analyze and collect statistics of such URL.
// Each HTTP probing's Result is sent over a channel that will be closed once
// all probings where made.
func (p *Prober) Run(url string) <-chan Result {
	out := make(chan Result)

	go func() {
		defer close(out)

		for {
			ctx, cancel := context.WithTimeout(context.Background(), p.Timeout)
			trace, err := Probe(ctx, http.MethodGet, url)
			cancel()

			out <- Result{trace, err}

			p.count++
			if p.count == p.Count {
				return
			}

			time.Sleep(p.Wait)
		}
	}()

	return out
}

// Probe returns a Trace of the HTTP probing against a target URL.
func Probe(ctx context.Context, method, url string) (*Trace, error) {
	if !strings.HasPrefix(url, "http://") && !strings.HasPrefix(url, "https://") {
		url = "http://" + url
	}

	req, err := http.NewRequest(method, url, nil)
	if err != nil {
		return nil, err
	}

	trace := new(Trace)
	resp, err := newTransport(trace).RoundTrip(req.WithContext(ctx))
	if err != nil {
		return nil, err
	}

	switch {
	case resp.StatusCode >= http.StatusInternalServerError:
		trace.status = probe.Failure
	case resp.StatusCode >= http.StatusBadRequest:
		trace.status = probe.Invalid
	case resp.StatusCode >= http.StatusOK:
		trace.status = probe.Success
	default:
		trace.status = probe.Unknown
	}

	return trace, nil
}
