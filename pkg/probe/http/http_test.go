package http

import (
	"context"
	"math/rand"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"

	"gitlab.com/jaguirre/probe/pkg/probe"
)

func TestProbeStatus(t *testing.T) {
	type testCase struct {
		name    string
		handler http.HandlerFunc
		status  probe.Status
	}

	testCases := make([]testCase, len(httpStatuses))
	for code, status := range httpStatuses {
		testCases = append(testCases, testCase{
			name: http.StatusText(code),
			handler: func(w http.ResponseWriter, r *http.Request) {
				w.WriteHeader(code)
			},
			status: status,
		})
	}

	ctx := context.Background()
	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			t.Parallel()

			s := httptest.NewServer(tc.handler)
			defer s.Close()

			trace, err := Probe(ctx, http.MethodGet, s.URL)
			if err != nil {
				t.Fatalf("Probe(%s) failed: %v", s.URL, err)
			}

			if trace.Status() != tc.status {
				t.Errorf("Probe(%s) unexpected status; got: %v, exp: %v", s.URL, trace.Status(), tc.status)
			}
		})
	}
}

func TestProbeTrace(t *testing.T) {
	ctx := context.Background()
	eps := 1 * time.Millisecond

	t.Run("ProcessingTime", func(t *testing.T) {
		func() {
			var elapsed time.Duration
			s := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
				start := time.Now()
				defer func() {
					elapsed = time.Since(start)
				}()
				time.Sleep(time.Duration(rand.Intn(100)) * time.Millisecond)
				w.WriteHeader(http.StatusOK)
			}))
			defer s.Close()

			trace, err := Probe(ctx, http.MethodGet, s.URL)
			if err != nil {
				t.Fatalf("Probe(%s) failed: %v", s.URL, err)
			}
			// Ensure we don't have an error greater than a eps
			if procTime := trace.ProcessingTime(); procTime-elapsed > eps {
				t.Errorf("Probe(%s) unpexpected ProcessingTime; got: %v, exp: %v", s.URL, procTime, elapsed)
			}
		}()
	})
}

var httpStatuses = map[int]probe.Status{
	http.StatusContinue:           probe.Unknown,
	http.StatusSwitchingProtocols: probe.Unknown,
	http.StatusProcessing:         probe.Unknown,

	http.StatusOK:                   probe.Success,
	http.StatusCreated:              probe.Success,
	http.StatusAccepted:             probe.Success,
	http.StatusNonAuthoritativeInfo: probe.Success,
	http.StatusNoContent:            probe.Success,
	http.StatusResetContent:         probe.Success,
	http.StatusPartialContent:       probe.Success,
	http.StatusMultiStatus:          probe.Success,
	http.StatusAlreadyReported:      probe.Success,
	http.StatusIMUsed:               probe.Success,

	http.StatusMultipleChoices:   probe.Success,
	http.StatusMovedPermanently:  probe.Success,
	http.StatusFound:             probe.Success,
	http.StatusSeeOther:          probe.Success,
	http.StatusNotModified:       probe.Success,
	http.StatusUseProxy:          probe.Success,
	http.StatusTemporaryRedirect: probe.Success,
	http.StatusPermanentRedirect: probe.Success,

	http.StatusBadRequest:                   probe.Invalid,
	http.StatusUnauthorized:                 probe.Invalid,
	http.StatusPaymentRequired:              probe.Invalid,
	http.StatusForbidden:                    probe.Invalid,
	http.StatusNotFound:                     probe.Invalid,
	http.StatusMethodNotAllowed:             probe.Invalid,
	http.StatusNotAcceptable:                probe.Invalid,
	http.StatusProxyAuthRequired:            probe.Invalid,
	http.StatusRequestTimeout:               probe.Invalid,
	http.StatusConflict:                     probe.Invalid,
	http.StatusGone:                         probe.Invalid,
	http.StatusLengthRequired:               probe.Invalid,
	http.StatusPreconditionFailed:           probe.Invalid,
	http.StatusRequestEntityTooLarge:        probe.Invalid,
	http.StatusRequestURITooLong:            probe.Invalid,
	http.StatusUnsupportedMediaType:         probe.Invalid,
	http.StatusRequestedRangeNotSatisfiable: probe.Invalid,
	http.StatusExpectationFailed:            probe.Invalid,
	http.StatusTeapot:                       probe.Invalid,
	http.StatusUnprocessableEntity:          probe.Invalid,
	http.StatusLocked:                       probe.Invalid,
	http.StatusFailedDependency:             probe.Invalid,
	http.StatusUpgradeRequired:              probe.Invalid,
	http.StatusPreconditionRequired:         probe.Invalid,
	http.StatusTooManyRequests:              probe.Invalid,
	http.StatusRequestHeaderFieldsTooLarge:  probe.Invalid,
	http.StatusUnavailableForLegalReasons:   probe.Invalid,

	http.StatusInternalServerError:           probe.Failure,
	http.StatusNotImplemented:                probe.Failure,
	http.StatusBadGateway:                    probe.Failure,
	http.StatusServiceUnavailable:            probe.Failure,
	http.StatusGatewayTimeout:                probe.Failure,
	http.StatusHTTPVersionNotSupported:       probe.Failure,
	http.StatusVariantAlsoNegotiates:         probe.Failure,
	http.StatusInsufficientStorage:           probe.Failure,
	http.StatusLoopDetected:                  probe.Failure,
	http.StatusNotExtended:                   probe.Failure,
	http.StatusNetworkAuthenticationRequired: probe.Failure,
}
